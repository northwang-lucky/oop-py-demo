from utils.process_ctl import show_menu, start_session, clear_screen
from handlers.student_handler import StudentHandler
from handlers.user_handler import UserHandler

# 创建一个student handler的类实例, 用来执行学生的CURD操作
student_handler = StudentHandler()

# 创建一个user handler的类实例, 用来执行用户的登录操作
user_handler = UserHandler()

# PS: 层级设计在 handlers/student_handler.py 中有说明


# 展示学生菜单 - return True表示继续循环, False表示退出循环
# 有一个入参叫loop_cnt, 这个表示当前是第几次循环, 是在start_session函数中传入的
def show_student_menu(loop_cnt):
    # 构造并打印学生菜单
    # show_menu函数里进行了如下操作:
    # 1. 根据传入的字典, 循环打印出菜单
    # 2. 执行input函数, 等待用户输入
    # 3. 验证用户输入的操作序号是否在字典规定的这些序号中, 如果没通过验证, 返回的第一个变量ok会是False
    # 4. 通过验证后, 返回的第二个变量opera_code就是用户输入的操作序号, 你可以根据其值做不同的操作
    ok, opera_code = show_menu({
        '1': '学生建档',
        '2': '修改学生',
        '3': '删除学生',
        '4': '查找学生',
        '5': '查看全部学生',
        '0': '退出程序',
    }, loop_cnt > 1)

    # 这里捕获了没通过输入验证的情况, 返回True表示继续循环, 即让用户重新输入
    if not ok:
        return True

    # 这里捕获了操作序号为0的情况, 返回False表示终止循环, 即退出程序
    if opera_code == '0':
        return False

    # 下面这一段是用来根据opera_code的不同值来做不同的操作的
    # 比如当opera_code为1时, 会调用"学生建档"的函数: student_handler.create()
    rst = {}
    if opera_code == '1':
        rst = student_handler.create()
    elif opera_code == '2':
        rst = student_handler.modify()
    elif opera_code == '3':
        rst = student_handler.remove()
    elif opera_code == '4':
        rst = student_handler.find()
    elif opera_code == '5':
        rst = student_handler.list()

    # 打印各个操作的执行结果 {'code': 0, 'message': '操作成功'} 之类的
    print(rst)
    # 因为上一个操作已经完成, 所以返回True表示继续循环, 即让用户重新选择要进行的操作
    return True


# 执行登录验证
def login(_):
    # 执行登录操作的函数
    rst = user_handler.login()
    # 打印操作结果
    print(rst)

    # 这里捕获了登录失败的情况, rst.code为0表示操作成功, 不为0即失败, 所以返回True表示继续循环, 即让用户重新登录
    if rst['code'] != 0:
        return True

    # 走到这里说明登录成功了, 清屏
    clear_screen()
    # 然后开始进入学生菜单, start_session函数的逻辑见下面的start_session(login)
    start_session(show_student_menu)
    return False


# 执行登录验证函数
# start_session函数用来执行一个循环, 该函数接收一个函数作为入参
# start_session会在内部的循环中执行这个作为入参的函数, 并通过其返回的布尔值来判断是否应该继续循环
# 作为入参的那个函数, 其返回值为True表示继续循环, False表示退出循环
start_session(login)
