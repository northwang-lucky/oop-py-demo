from entities.user import User
from utils.search import find_index
from utils.result import success, failure

# 本文件是用户登录操作的核心封装
# 拆解一下类名UserProvider:
# User是用户, Provider是提供者, 合在一起就代表, 这个类提供了User类相关的核心功能


class UserProvider:

    # 存放用户信息, 预定义一个用户
    __users = [User('admin', '123456')]

    # 登录函数
    def login(self, user):
        # 从self.__users中查找出用户名和密码均正确的用户
        # lambda u: u.username == user.username and u.password == user.password 这一句看着挺唬人,
        # 但实际上换成js的写法就是个箭头函数: u => u.username === user.username && u.password === user.password
        target_index = find_index(
            self.__users,
            lambda u: u.username == user.username and u.password == user.password,
        )
        # 三目运算符, 相当于 target_index == -1 ? failure('用户名或密码错误!') : success('登录成功')
        return failure('用户名或密码错误!') if target_index == -1 else success('登录成功')
