from utils.result import success, failure
from utils.search import find_index

# 本文件是学生CURD操作的核心封装
# 拆解一下类名StudentProvider:
# Student是学生, Provider是提供者, 合在一起就代表, 这个类提供了Student类相关的核心功能


class StudentProvider:

    # 存放学生对象
    __students = []

    # 检查学生是否已经存在
    def check(self, stu_no):
        # 从self.__students中查找出学号和传入的学号相同的学生
        # lambda stu: stu.stu_no == stu_no 这一句看着挺唬人,
        # 但实际上换成js的写法就是个箭头函数: stu => stu.stu_no === stu_no
        target_index = find_index(self.__students, lambda stu: stu.stu_no == stu_no)

        if target_index == -1:
            return False, target_index, failure('该生不存在')

        return True, target_index, failure('该生已存在')

    # 新增一个学生
    def create(self, new_stu):
        existed, _, failure_rst = self.check(new_stu.stu_no)
        if existed:
            return failure_rst

        self.__students.append(new_stu)
        return success()

    # 删除一个学生
    def remove(self, stu_no):
        existed, target_index, failure_rst = self.check(stu_no)
        if not existed:
            return failure_rst

        del self.__students[target_index]
        return success()

    # 修改一个学生
    # target_index是可选参数, 如果传了就直接替换该索引位置的学生
    def modify(self, new_stu_info, target_index=None):
        if target_index is None:
            existed, target_index, failure_rst = self.check(new_stu_info.stu_no)

            if not existed:
                return failure_rst

        # 这里需要对传入的new_stu_info对象的每个字段挨个判空, 只有不为空的才需要更新
        target_stu = self.__students[target_index]
        if new_stu_info.name is not None and new_stu_info.name != '':
            target_stu.name = new_stu_info.name
        if new_stu_info.gender is not None and new_stu_info.gender != '':
            target_stu.gender = new_stu_info.gender
        if new_stu_info.age is not None and new_stu_info.age != '':
            target_stu.age = new_stu_info.age

        # 替换当前索引对应位置的学生信息
        self.__students[target_index] = target_stu
        return success()

    # 查找单个学生
    def find(self, stu_no):
        existed, target_index, failure_rst = self.check(stu_no)
        if not existed:
            return failure_rst

        stu = self.__students[target_index]
        print(f'\n学号：{stu.stu_no}\n姓名：{stu.name}\n性别：{stu.gender}\n年龄：{stu.age}')
        return success()

    # 查找所有学生
    def list(self):
        for i in range(0, self.__students.__len__()):
            stu = self.__students[i]
            print(f'{i+1})\n学号：{stu.stu_no}\n姓名：{stu.name}\n性别：{stu.gender}\n年龄：{stu.age}\n')
        return success()
