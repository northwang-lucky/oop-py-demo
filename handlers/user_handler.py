from providers.user_provider import UserProvider
from entities.user import User


class UserHandler:

    __provider = UserProvider()

    def login(self):
        username = input('用户名: ')
        password = input('密码: ')
        return self.__provider.login(User(username, password))
