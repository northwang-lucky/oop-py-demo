from providers.student_provider import StudentProvider
from entities.student import Student

# 本文件是学生CURD操作的交互封装
# 拆解一下类名StudentHandler:
# Student是学生, Handler是处理器, 合在一起就代表, 这个类提供了Student类相关的交互处理功能

# 顺便在这里科普下层级架构的设计:
# main (程序入口) -> handler (处理交互逻辑) -> provider (处理核心逻辑)
#   |                 |                        |
# (utils)         (utils, entities)      (utils, entities)
# 任何层级都可以用到 utils (工具函数), handler和provider层级会用到 entities (实体类)


class StudentHandler:
    # 学号累加的变量, 用一次增加一次
    __stu_no_gen = 1

    # 处理交互的时候肯定会用到CURD的核心功能, 所以需要创建Provider的实例, 以调用其封装好的CURD函数
    __provider = StudentProvider()

    def create(self):
        # 生成一个学号, 这个函数返回了两个东西, 第一个是生成的学号, 第二个是一个让学号增长的函数
        stu_no, stu_no_increase = self.__gen_stu_no()
        # 接收用户输入
        name = input('姓名：')
        gender = input('性别：')
        age = input('年龄：')
        # 调用核心逻辑, 将学生加进去
        rst = self.__provider.create(Student(stu_no, name, gender, age))
        # 调用成功, 将学号增长一位
        if rst['code'] == 0:
            stu_no_increase()
        # 返回执调用结果
        return rst

    def remove(self):
        stu_no = input('要删除的学生的学号：')
        return self.__provider.remove(stu_no)

    def modify(self):
        stu_no = input('要修改的学生的学号：')
        existed, target_index, failure_rst = self.__provider.check(stu_no)
        if not existed:
            return failure_rst

        print('\n系统已成功匹配该生信息，下面开始修改流程。\n注意，以下表单项如留空则表示不修改该项!\n')
        name = input('新的姓名：')
        gender = input('新的性别：')
        age = input('新的年龄：')

        return self.__provider.modify(Student(stu_no, name, gender, age), target_index)

    def find(self):
        stu_no = input('要查找的学生的学号：')
        return self.__provider.find(stu_no)

    def list(self):
        return self.__provider.list()

    def __gen_stu_no(self):
        def stu_no_increase():
            self.__stu_no_gen += 1

        return str(self.__stu_no_gen), stu_no_increase
