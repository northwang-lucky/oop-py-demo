import os

# 本文件是和进程控制相关的函数封装


# 开始会话
def start_session(callback):
    toggle = True
    count = 1
    # 开始一个必然会循环一次的循环, 是否继续循环由callback函数的返回值决定
    while toggle:
        toggle = callback(count)
        count += 1


# 展示一个菜单
def show_menu(menu, start_with_newline=False):
    # 用于控制是否先换行再打印
    if start_with_newline:
        print()

    # 遍历并打印菜单
    for opera_code, desc in menu.items():
        print(f'{opera_code}: {desc}')

    # 接收用户输入
    opera_code = input('要执行的操作序号：')
    # 校验输入的操作序号是否正确
    if opera_code not in menu.keys():
        print('操作序号输入有误，请重新输入！')
        return False, None
    return True, opera_code


# 清屏
def clear_screen():
    # 相当于在终端输入clear
    os.system('clear')
