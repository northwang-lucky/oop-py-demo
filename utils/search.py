# 本文件是搜索相关函数的封装


# 查找符合条件的元素的索引值
def find_index(list, callback):
    target_index = -1
    for i in range(0, list.__len__()):
        if callback(list[i]):
            target_index = i
            break
    return target_index
