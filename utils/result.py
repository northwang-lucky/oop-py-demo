from time import strftime, localtime

# 本文件封装了返回结果相关的函数


# 获取并格式化当前时间
def now():
    return strftime('%Y-%m-%d %H:%M:%S', localtime())


# 构造一个成功的返回结果字典, message是可选参数
def success(message=None):
    rst = {
        'code': 0,
        'datetime': now()
    }
    if message is not None:
        rst['message'] = message
    else:
        rst['message'] = '操作成功'
    return rst


# 构造一个失败的返回结果字典
def failure(message):
    return {
        'code': -1,
        'datetime': now(),
        'message': message,
    }
