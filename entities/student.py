class Student:
    stu_no = None
    name = None
    gender = None
    age = None

    def __init__(self, stu_no, name, gender, age):
        self.stu_no = stu_no
        self.name = name
        self.gender = gender
        self.age = age
